#!/usr/bin/env python3
import sys
from pathlib import Path
import unittest
thisDir=Path(__file__).parent.absolute()
sys.path.append(str(thisDir.parent))
modelsPrefix=thisDir / "models"

import numpy as np
import pandas
from pandas.testing import assert_frame_equal, assert_series_equal
import AutoXGBoost as axgb
from AutoXGBoost import *
import UniOpt

from os import urandom
import struct
from pprint import pprint
from collections import OrderedDict

vectorCount=100
vectorLen=2

def randomVector():
	np.random.rand(vectorLen)

def randomDataset(*args, **kwargs):
	ds=np.random.rand(vectorCount, vectorLen)
	cns=["A"+str(a) for a in range(vectorLen)]
	spec={cn:"numerical" for cn in cns}
	ds=pandas.DataFrame(ds, columns=cns)
	return axgb.AutoXGBoost(spec, ds, prefix=modelsPrefix, *args, **kwargs)

class SimpleTests(unittest.TestCase):
	@classmethod
	def setUpClass(cls):
		cls.ds=randomDataset()
	
	def testTrain(self):
		columnsCountStr=str(len(self.__class__.ds.columns))
		self.assertEqual(self.__class__.ds.getIssues(), {'No data': False, 'stop columns processed': {}, 'HP unknown': set(self.__class__.ds.columns), 'HP known, but no models': set(), 'not scored': set()})
		
		self.__class__.ds.loadHyperparams()
		self.assertEqual(self.__class__.ds.getIssues(), {'No data': False, 'stop columns processed': {}, 'HP unknown': set(), 'HP known, but no models': set(self.__class__.ds.columns), 'not scored': set()})
		
		self.__class__.ds.trainModels()
		self.assertEqual(self.__class__.ds.getIssues(), {'No data': False, 'stop columns processed': {}, 'HP unknown': set(), 'HP known, but no models': set(), 'not scored': set(self.__class__.ds.columns)})
		
		self.__class__.ds.scoreModels()
		okDict={'No data': False, 'stop columns processed': {}, 'HP unknown': set(), 'HP known, but no models': set(), 'not scored': set()}
		self.assertEqual(self.__class__.ds.getIssues(), okDict)
		self.assertEqual(repr(self.__class__.ds), self.__class__.ds.__class__.__name__+"< columns: "+columnsCountStr+", numerical: "+columnsCountStr+" | OK >")
		self.assertEqual(self.__class__.ds.getIssues(), okDict)
		
		self.__class__.ds.sortScores()
		self.assertEqual(self.__class__.ds.getIssues(), okDict)
		
		print(self.__class__.ds.scores)
		self.assertEqual(self.__class__.ds.getIssues(), okDict)
		
		self.__class__.ds.saveModels(format=formats.binary)
		print(self.__class__.ds.predict("A1"))

class SavingDumbTests(unittest.TestCase):
	@classmethod
	def setUpClass(cls):
		cls.ds=randomDataset()
		cls.ds.loadHyperparams()
		cls.ds.trainModels()
		cls.ds.scoreModels(folds=3)
		cls.ds.sortScores()
	
	def testSaveFormat(self):
		for modelClass in models:
			for format in formats:
				with self.subTest(modelClass=modelClass, format=format):
					self.__class__.ds.saveModels(format=format)

class PrepareDatasetForConversionTests(unittest.TestCase):
	@classmethod
	def setUpClass(cls):
		cls.ds=randomDataset()
		cls.ds.loadHyperparams()
		cls.ds.trainModels()
		cls.ds.scoreModels(folds=5)
		cls.ds.sortScores()
		cls.colName="A0"
		cls.model=cls.ds.models[cls.colName]
		cls.tv=cls.ds.prepareCovariates(cls.colName)
		cls.etalonRes=cls.model.predict(cls.tv)

class TestsConversionFromNative(PrepareDatasetForConversionTests):
	def testConversion(self):
		m=self.__class__.model
		for modelClass in models:
			converted=m.convert(modelClass)
			if converted:
				converted.predict(self.__class__.tv)

class DifferrentFormatsTest(PrepareDatasetForConversionTests):
	@classmethod
	def setUpClass(cls):
		super().setUpClass()
		
		cls.colName="A0"
		m=cls.ds.models[cls.colName]
		cls.modelsInDifferentFormats={}
		for modelClass in models:
			cls.modelsInDifferentFormats[modelClass]=m.convert(modelClass)

class ConvertingTests(DifferrentFormatsTest):
	def testConversion(self):
		ds=self.__class__.ds
		for frm in models:
			for to in models:
				with self.subTest(frm=frm, to=to):
					self.__class__.modelsInDifferentFormats[frm].convert(to)

class SavingTests(DifferrentFormatsTest):
	def testSavingFromDifferent(self):
		for modelClass in models:
			for format in formats:
				m=self.__class__.modelsInDifferentFormats[modelClass]
				cn=self.__class__.colName
				with self.subTest(modelClass=modelClass, format=format):
					if format.ctor in modelClass.exports.keys():
						m.save(cn, None, format=format)

class LoadingTests(DifferrentFormatsTest):
	@classmethod
	def setUpClass(cls):
		cls.ds=randomDataset()
		cls.ds.loadHyperparams()
		cls.ds.trainModels()
		cls.ds.scoreModels(folds=5)
		cls.ds.sortScores()
		for format in formats:
			cls.ds.saveModels(format=format)
	
	def testLoadFormat(self):
		for modelClass in models:
			for format in formats:
				with self.subTest(modelClass=modelClass, format=format):
					if format in modelClass.imports:
						self.__class__.ds.loadModels(format=format, modelCtor=modelClass)
					else:
						with self.assertRaises(axgb.core.UnsupportedFormatException) as cm:
							self.__class__.ds.loadModels(format=format, modelCtor=modelClass)
						self.assertEqual(cm.exception.args, ('load', format, modelClass))
	
	#@unittest.skip
	def testSaveLoadAutoGuessFormat(self):
		for format in formats:
			if format.ctor:
				with self.subTest(format=format, ctor=format.ctor):
					modelClass=format.ctor
					if format in modelClass.imports:
						self.__class__.ds.loadModels(format=format, modelCtor=modelClass)
					else:
						with self.assertRaises(axgb.core.UnsupportedFormatException) as cm:
							self.__class__.ds.loadModels(format=format, modelCtor=modelClass)
						self.assertEqual(cm.exception.args, ('load', format, modelClass))

#@unittest.skip
class OptimizersTests(unittest.TestCase):
	@classmethod
	def setUpClass(cls):
		cls.ds=randomDataset()
	
	@unittest.skip
	def testOptimizers(self):
		results={}
		for optimizer in UniOpt:
			with self.subTest(optimizer=optimizer):
				self.__class__.ds.optimizeHyperparams(iters=20, optimizer=optimizer, force=True)
				self.__class__.ds.trainModels()
				self.__class__.ds.scoreModels()
				results[optimizer]=next(reversed(self.__class__.ds.scores.values()))
		results=OrderedDict(((k.__name__, v) for k,v in sorted(results.items(), key=lambda x: x[1][0])))
		pprint(results)
	
	#@unittest.skip
	def testOptimizer(self):
		"""
		UniOpt.backends.GPyOpt.GPyOpt
		UniOpt.backends.pySOT.PySOT
		"""
		self.__class__.ds.optimizeHyperparams(iters=20, optimizer=UniOpt.GPyOpt, force=True)

if __name__ == '__main__':
	unittest.main()