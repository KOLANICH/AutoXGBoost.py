__all__=("AutoXGBoost", "AutoXGBoostImputer", "Model", "formats", "models")
from collections import OrderedDict
from pathlib import Path
import os
from Chassis import Chassis, MissingColumnsExplainer
from .imports import *
from .defaults import *

from .core import OFDT, Model, hyperparamsTypeCoerce, NotAModelException, UnsupportedFormatException, formatFromFileName
from .models import XGBoostModel
from . import formats
from .crossvalidation import getLastMetricFromMetricsDict, XGBoostNativeCV
from .utils import parseDict, serializeDict
import warnings

gridSpecModule=lazyImport(".gridSpec")

from lazily import UniOpt
from lazily.sklearn.model_selection import train_test_split
import pathvalidate

#core.formatExtMapping:Mapping[str, OFDT]={f.ext:f for f in formats if f.ext}
#core.ctorFormatMapping:Mapping[Type[Model], OFDT]={f.ctor:f for f in formats if f.ext}
glue.formatExtMapping.update({f.ext:f for f in formats if f.ext})
glue.ctorFormatMapping.update({f.ctor:f for f in formats if f.ext})

class AutoXGBoost(Chassis):
	"""The class doing most of preparing data to regresion."""
	#bestHyperparams:Mapping[str, Mapping[str, object]]
	#models:Mapping[str, Model]
	#scores:Mapping[str, Tuple[float, float]]
	#modelsPrefix:Path
	#hyperparamsFile:Path
	
	groupsTypes=type(Chassis.groupsTypes)(Chassis.groupsTypes)
	groupsTypes.update(defaults.categoriesSubtypes)
	
	def __init__(self, spec: Mapping[str, str], dataset: pandas.DataFrame, params: typing.Optional[Mapping[str, object]] = None, prefix: typing.Optional[Path] = None) -> None:
		"""
		`params` are default params, including hyperparams,
		`prefix` is the path to save and load model files from/to
		"""
		if params is None:
			params=type(defaultParams)(defaultParams)
		
		super().__init__(spec, dataset)
		self.params=params
		self.bestHyperparams={}
		self.models={}
		self.scores=OrderedDict()
		if prefix is None:
			prefix=defaultModelsPrefix
		self.modelsPrefix=Path(prefix)
		self.hyperparamsFile=self.modelsPrefix / defaultHyperparamsFileName
	
	def getMissingHyperparams(self) -> Set[str]:
		return self.columns-set(self.bestHyperparams.keys())
	
	def getIssues(self) -> Dict[str, Union[bool, Set[str]]]:
		"""Diagnoses problems in the state of the tool"""
		stopColumnsProcessed={}
		for pn in ("bestHyperparams", "models", "scores"):
			scIn=self.groups["stop"]&set(getattr(self, pn).keys())
			if scIn:
				stopColumnsProcessed[pn]=scIn
		
		return {
			"No data": self.pds is None or not bool(len(self.pds)),
			"stop columns processed": stopColumnsProcessed,
			"HP unknown": self.getMissingHyperparams(),
			"HP known, but no models": set(self.bestHyperparams.keys())-set(self.models.keys()),
			"not scored": set(self.models.keys())-set(self.scores.keys()),
		}
	
	def _reprContents(self) -> str:
		prRes=[]
		for prN, prC in self.getIssues().items():
			if prC:
				if not isinstance(prC, bool):
					prRes.append( prN+": "+str(prC) )
				else:
					prRes.append( prN )
		
		return super()._reprContents() + ( (" ! "+ ", ".join(prRes)) if prRes  else " | OK" )
	
	def getMetric(self, cn: str) -> str:
		"""Returns loss metric suitable for the data type"""
		return metricsMapping[self.features[cn]]
	
	def prepareCrossValidation(self, cn: str, hyperparams: typing.Optional[dict] = None, metric: typing.Optional[str] = None, testShare:float=0.) -> XGBoostNativeCV:
		if metric is None:
			metric=self.getMetric(cn)
		return XGBoostNativeCV(self, cn, hyperparams=hyperparams, metrics=(metric,), testShare=testShare)
	
	def getErr(self, cn: str, folds: int = 10, hyperparams: typing.Optional[dict] = None, metric: typing.Optional[str] = None) -> Tuple[np.float64, np.float64]:
		return getLastMetricFromMetricsDict(self.prepareCrossValidation(cn, hyperparams=hyperparams, metric=metric).evaluate(folds=folds, hyperparams=hyperparams))

	def prepareFitting(self, cn: str, testShare:float=0, strata=None) -> Tuple[pandas.DataFrame, pandas.Series, Dict[str, Union[int, str]]]:
		dmat=self.pds[self.colNotNA(cn)]
		paramz=type(self.params)(self.params)
		x = self.prepareCovariates(cn, dmat, correctOrder=False)
		paramz.update(depParams[self.features[cn]])
		y=self.prepareResult(cn, dmat)
		if cn in self.catRemap:
			#paramz['num_class'] = len(y.value_counts().index)
			paramz['num_class'] = len(self.catRemap[cn])
			y=y.cat.codes
		#rebalancer=SMOTE()
		#rebalancer._force_all_finite = False
		#(x, y) = rebalancer.fit_sample(x, y) #FUCK, doesn't tolerate missing values
		
		testSet=None
		if testShare:
			if strata:
				strata=x[strata]
			trainX, testX, trainY, testY=train_test_split(x, y, test_size=testShare, stratify=strata)
			trainSet=(trainX, trainY)
			testSet=(testX, testY)
		else:
			trainSet=(x, y)
		
		return (trainSet, testSet, paramz)

	def getSklearnModelClass(self, cn:str):
		"""Returns xgboost.sklearn class suitable for this variable type"""
		if cn in self.groups["categorical"]:
			return xgb.sklearn.XGBClassifier
		else:
			return xgb.sklearn.XGBRegressor
	
	def bestNumberOfTreesFromCV(self, cn, hyperparams, folds, maxBoostRounds, cv=None):
		if cv is None:
			cv=self.prepareCrossValidation(cn)
		hpNew = type(hyperparams)(hyperparams)
		hpNew[ 'num_boost_round'] = maxBoostRounds
		fromNumTrees=cv.evaluate(folds=folds, hyperparams=hpNew)
		for mName in fromNumTrees:
			return np.argmin(fromNumTrees[mName]["test"]["mean"])+1
	
	def getBestHyperparamsForTheFeature(self, cn:str, folds:int=10, iters:int=1000, jobs:int=3, optimizer:Type["UniOpt.core.Optimizer"]=None, gridSpec:typing.Optional[Mapping[str, object]]=None, testShare:float=0., pointsStorage:"UniOpt.corePointsStorage"=None, stagesMaxRounds=(10, 10000), *args, **kwargs):
		"""A dispatcher method optimizing hyperparams for the column. method is a string used to choose a method"""
		if gridSpec is None:
			gridSpec=dict(gridSpecModule.defaultGridSpec)
		
		if optimizer is None:
			optimizer=UniOpt.MSRSM
		
		gridSpec['num_boost_round']=stagesMaxRounds[0]
		
		cv=self.prepareCrossValidation(cn, testShare=testShare)
		
		def doCv(hyperparams):
			res=cv.evaluate(folds=folds, hyperparams=hyperparams)
			res=getLastMetricFromMetricsDict(res)
			return res
		
		opt=optimizer(doCv, gridSpec, iters=iters, jobs=jobs, pointsStorage=pointsStorage, *args, **kwargs)
		
		#TODO: use the following line for hyperband
		#hyperparams["max_depth"]+=round(math.log2(n_iterations))
		
		best=opt()
		self.details=opt.details
		
		best['num_boost_round']=self.bestNumberOfTreesFromCV(cn, best, folds, stagesMaxRounds[1], cv=cv)
		
		#print("best: ", best)
		#print("details: ", self.details)
		
		hyperparamsTypeCoerce(best)
		assert best is not None
		return best
	
	def loadHyperparams(self, fileName:typing.Optional[Path] = None) -> None:
		if fileName is None:
			fileName=self.hyperparamsFile
		self.bestHyperparams=parseDict(fileName)
	
	def saveHyperparams(self, fileName:typing.Optional[Path]=None):
		if fileName is None:
			fileName=self.hyperparamsFile
		serializeDict(fileName, self.bestHyperparams)

	def optimizeHyperparams(self, autoSave:bool=True, folds:int=10, iters:int=1000, jobs:int=None, optimizer:Type["UniOpt.core.Optimizer"]=None, columns:typing.Optional[Set[str]]=None, force:Optional[bool]=None, testShare:float=0., pointsStorage:"UniOpt.corePointsStorage"=None, *args, **kwargs):
		"""Optimizes hyperparams.
		AutoSave saves best hyperparms after they were estimated for each feature. It's a safety measure for the case of crashes.
		"""
		if columns is None:
			columns=self.columns
		presentHyperparams=columns&set(self.bestHyperparams)
		missingHyperparams=self.getMissingHyperparams()
		
		hyperparamsToCompute=missingHyperparams&columns
		if presentHyperparams:
			if force is None:
				warnings.warn("Set `force` into True or False to explicitly clarify if you wanna recompute params which are already present.")
			if force:
				hyperparamsToCompute|=presentHyperparams
		
		with mtqdm(hyperparamsToCompute, desc="optimizing hyperparams", unit="model") as pb:
			for cn in pb:
				pb.write(cn)
				#try:
				self.bestHyperparams[cn]=self.getBestHyperparamsForTheFeature(cn, folds=folds, iters=iters, jobs=jobs, optimizer=optimizer, testShare=testShare, pointsStorage=pointsStorage, *args, **kwargs)
				if autoSave:
					self.saveHyperparams()
				#except Exception as e:
				#	pb.write(" ".join((cn, str(e))))
				#	#raise e
	
	def getModelDefaultFileName(self, cn:str, format:OFDT, prefix:Path=None) -> Path:
		if prefix is None:
			prefix = self.modelsPrefix
		return prefix/(pathvalidate.sanitize_filename(cn, "_")+"."+format.ext)
	
	def loadModel(self, fn: Path=None, cn:str=None, prefix: typing.Optional[Path] = None, format: typing.Optional[OFDT] = None, modelCtor: typing.Type[Model] = None) -> None:
		"""Loads model into an object. Each model is an xgboost binary file accompanied with a json file containing the metadata:
			columns names, their order, their types, and some info about the model.
			if you wanna XGBoost sklearn-like interface, set modelCtor to SKLearnXGBoostModel
		"""
		if isinstance(format, str):
			format=getattr(formats, format)
		
		if fn is not None:
			if format is None:
				format=formatFromFileName(fn)
		
		if fn is None:
			if format:
				fn = self.getModelDefaultFileName(cn, format, prefix)
			else:
				if prefix is None:
					prefix=self.modelsPrefix
				for fnCandidate in prefix.glob(pathvalidate.sanitize_filename(cn, "_")+".*"):
					if format is None:
						format=formatFromFileName(fnCandidate)
						fn=fnCandidate
						if format is not None:
							break
		
		if modelCtor is None:
			if format and format.ctor:
				modelCtor=format.ctor
			else:
				raise UnsupportedFormatException("load", format, format.ctor)
		
		self.loadModel_(fn=fn, cn=cn, prefix=prefix, format=format, modelCtor=modelCtor)
	
	def loadModel_(self, fn: Path=None, cn:str=None, prefix: typing.Optional[Path] = None, format: typing.Optional[OFDT] = None, modelCtor: typing.Type[Model] = None) -> None:
		m=modelCtor(self)
		m.open(fn, cn, prefix, format)
	
	def convertModel(self, cn:typing.Optional[str], modelCtor:typing.Type[Model]) -> None:
		self.models[cn]=self.models[cn].convert(modelCtor)
	
	def convertModels(self, modelCtor:typing.Type[Model]) -> None:
		for cn in self.models:
			convertModel(cn, modelCtor)
	
	def loadModels_(self, prefix:typing.Optional[Path], format:OFDT=None, modelCtor:typing.Type[Model] = None):
		"""Loads all models of specified format from the dir"""
		modelsFileNames=[fn for fn in prefix.glob("*."+format.ext)]
		for fn in modelsFileNames:
			try:
				self.loadModel(fn=fn, prefix=prefix, format=format, modelCtor=modelCtor)
			except NotAModelException:
				pass
	
	def loadModels(self, prefix:typing.Optional[Path]=None, format:typing.Optional[OFDT]=None, modelCtor:typing.Type[Model] = None):
		if prefix is None:
			prefix=self.modelsPrefix
		if format is None:
			for format in formats:
				self.loadModels_(prefix, format, modelCtor)
		else:
			self.loadModels_(prefix, format, modelCtor)
		
		self.sortScores()
	
	def saveModels(self, prefix:typing.Optional[Path]=None, format:typing.Optional[OFDT]=None):
		"""Saves models for all the columns. format allos to serialize models into other formats than binary."""
		if prefix is None:
			prefix=self.modelsPrefix
		
		with mtqdm(self.models.items(), unit="model", desc="saving models") as pb:
			for cn, m in pb:
				pb.write(cn)
				m.save(cn, fn=None, prefix=prefix, format=format)
	
	def setupEarlyStoppingAndTestSet(self, testSet, pythonHyperparams, total=None):
		if testSet:
			testMat=xgb.DMatrix(testSet[0], label=testSet[1], nthread=-1)
			if "evals" not in pythonHyperparams:
				pythonHyperparams["evals"]=[]
			
			pythonHyperparams["evals"].append((testMat, "earlyStop"))
		
		pythonHyperparams["early_stopping_rounds"]=10
	
	def trainModel(self, cn: str, testShare=0., strata=None, **kwargs) -> XGBoostModel:
		"""Trains a model to predict column with the name cn"""
		
		((x, y), testSet, paramz) = self.prepareFitting(cn, testShare=testShare, strata=strata)
		
		if cn in self.bestHyperparams:
			paramz.update(self.bestHyperparams[cn])
		
		hyperparamsTypeCoerce(paramz)
		
		modelClass=XGBoostModel
		pythonHyperparams=modelClass.remapPythonHyperparams(paramz)
		pythonHyperparams.update(kwargs)
		
		xm=xgb.DMatrix(x, label=y, nthread=-1)
		
		if "callbacks" not in pythonHyperparams:
			pythonHyperparams["callbacks"]=[]
		
		if "verbose_eval" not in pythonHyperparams:
			pythonHyperparams["verbose_eval"]=True
		
		total=None
		for roundsParamName in ("num_boost_round", "n_estimators"):
			if roundsParamName in pythonHyperparams:
				total=pythonHyperparams[roundsParamName]
				break
		
		if total is None:
			total=pythonHyperparams["num_boost_round"]=50
		
		if testSet:
			self.setupEarlyStoppingAndTestSet(testSet, pythonHyperparams, total)
		
		if pythonHyperparams["verbose_eval"] is True:
			pythonHyperparams["verbose_eval"]=total//10
		if isinstance(pythonHyperparams["verbose_eval"], int):
			printInfoEachNIters=pythonHyperparams["verbose_eval"]
		else:
			printInfoEachNIters=0
		pythonHyperparams["verbose_eval"]=False
		
		with mtqdm(total=total, desc="fitting model for col "+cn, unit="round") as pb:
			showedI=-1
			def progressCallback(env):
				nonlocal showedI
				i = env.iteration
				delta=i-showedI
				showedI=i
				
				if env.evaluation_result_list and printInfoEachNIters and i % printInfoEachNIters == 0:
					pb.write(repr(dict(env.evaluation_result_list)))
				pb.update(delta)

			pythonHyperparams["callbacks"].append(progressCallback)
			m=xgb.train(paramz, xm, **pythonHyperparams)
		
		res=modelClass(self, m)
		res.columnName=cn
		return res
	
	def trainModels(self, columns:typing.Optional[Iterable[str]]=None, **kwargs) -> None:
		"""Trains an xgboost models to predict columns based on the rest of columns"""
		if columns is None:
			columns=self.bestHyperparams
			
		with mtqdm(columns, desc="training models", unit="model") as pb:
			for cn in pb:
				pb.write(cn)
				self.models[cn]=self.trainModel(cn)
	
	def sortScores(self) -> None:
		"""Sorts scores from the best to the worst"""
		self.scores=type(self.scores)(sorted(self.scores.items() ,key=lambda x: abs(x[1][0])*100+abs(x[1][1])))
	
	def scoreModels(self, folds:int=20, columns:typing.Optional[Iterable[str]]=None) -> None:
		"""Computes errors for each model using n-fold crossvalidation."""
		if columns is None:
			columns=self.models
		with mtqdm(columns, desc="scoring models") as pb:
			for cn in pb:
				pb.write(cn)
				self.scores[cn]=self.getErr(cn, folds=folds)
				scale=typeScoreCoefficients[self.features[cn]](self, cn)**2
				if scale:
					self.scores[cn]=(self.scores[cn][0]/scale, self.scores[cn][1]/scale)
		self.sortScores()
		self.scores=OrderedDict(self.scores)
	
	def prepareCovariates(self, cn:str, dmat:typing.Optional[pandas.DataFrame]=None, correctOrder:bool=True) -> pandas.DataFrame:
		x=super().prepareCovariates(cn, dmat)
		if cn in self.models and correctOrder:
			m=cast(XGBoostModel, self.models[cn])
			featureNames=m.featureNames
			
			missingColumnsInDmat = set(featureNames) - set(x.columns)
			mce=MissingColumnsExplainer(self, missingColumnsInDmat)
			mce.fix(x)
			
			try:
				with warnings.catch_warnings():
					warnings.simplefilter("ignore")
					x=x.loc[:, featureNames]
			except:
				x=x.reindex(featureNames)
		return x
	
	def preparePrediction(self, cn: str, dmat:typing.Optional[Chassis]=None, **kwargs):
		if dmat is None:
			dmat=self
		obj=None
		if isinstance(dmat, pandas.DataFrame):
			dmat=dmat
			obj=self
		elif isinstance(dmat, Chassis):
			obj=dmat
			dmat=dmat.pds
		else:
			raise Exception("dmat must be either a pandas.DataFrame or a Chassis")
		
		x=self.prepareCovariates(cn, dmat, correctOrder=True)
		m=cast(XGBoostModel, self.models[cn])
		return x, m
	
	def predict(self, cn: str, dmat:typing.Optional[Chassis]=None, argmax:bool=True, returnPandas:bool=None, SHAPInteractions:typing.Optional[bool]=None, **kwargs) -> np.ndarray:
		"""Predicts column values based on the matrix dmat. argmax controls whether we should apply argmax for one-hot encoded categorical variables rather than return a probability vector."""
		x, m = self.preparePrediction(cn, dmat, **kwargs)
		res=m.predict(x, **kwargs)
		
		if SHAPInteractions is not None:
			shapRes=self.computeSHAPValues(cn=cn, dmat=None, argmax=argmax, returnPandas=returnPandas, SHAPInteractions=SHAPInteractions, _xm=(x, m), **kwargs)
		
		if not isinstance(x.index, pandas.core.indexes.range.RangeIndex) or x.index._start != 0:
			if returnPandas is False:
				warnings.warn("Source dataset uses discontinued index. Make sure that the source array is sorted by index (sort_index in pandas).")
			else:
				returnPandas=True
			
		#print(res)
		if cn in self.groups["categorical"] and argmax:
			res=self.oneHotToCategory(cn, res, x.index)
			if returnPandas is False:
				res=res.as_matrix()
		else:
			if returnPandas is True:
				res=self.numpyToColumn(cn, res, x.index)
		
		if SHAPInteractions is None:
			return res
		else:
			return res, shapRes
	
	def computeSHAPValues(self, cn: str, dmat:typing.Optional[Chassis]=None, argmax:bool=True, returnPandas:bool=None, SHAPInteractions:typing.Optional[bool]=False, _xm=None, **kwargs):
		"""Computes shap values and interactions"""
		if _xm is None:
			_xm = self.preparePrediction(cn, dmat, **kwargs)
		x, m = _xm
		
		if SHAPInteractions is False:
			shapValues = m.predict(x, pred_contribs=True, **kwargs)
		elif SHAPInteractions is True:
			shapValues = m.predict(x, pred_interactions=True, **kwargs)
		
		if not isinstance(x.index, pandas.core.indexes.range.RangeIndex) or x.index._start != 0:
			if returnPandas is False:
				warnings.warn("Source dataset uses discontinued index. Make sure that the source array is sorted by index (sort_index in pandas).")
			else:
				returnPandas = True
		
		if not SHAPInteractions:
			biasCol = shapValues[:, -1]
			shapValues = shapValues[:, :-1]
			if returnPandas is True:
				shapValues = pandas.DataFrame(shapValues, columns=m.featureNames)
		else:
			biasCol = shapValues[:, -1]
			biasRow = shapValues[-1, :]
			shapValues = shapValues[:-1, :-1]
			if returnPandas is True:
				warnings.warn("SHAP interactions is 3d stuff unsuitable for pandas")
			
		#print(res)
		if argmax:
			warnings.warn("Not sure that I have implemented combining categorical SHAP values correctly. There may be problems")
			shapValues = self.reduceCategoricalCols(shapValues)
			if returnPandas is False:
				shapValues = shapValues.as_matrix()
		
		if not SHAPInteractions:
			return (shapValues, biasCol)
		else:
			return (shapValues, biasCol, biasRow)
	
	def SHAPForcePlot(self, shapValuesAndBiases=None, cn: str=None, dmat:typing.Optional[Chassis]=None, argmax:bool=True, _xm=None, **kwargs):
		import shap
		if shapValuesAndBiases is None:
			shapValuesAndBiases = self.computeSHAPValues(self, cn=cn, dmat=dmat, argmax=argmax, returnPandas=True, SHAPInteractions=False, **kwargs)
		shapValues, biases = shapValuesAndBiases
		return shap.force_plot(biases[0], shapValues.values, feature_names=shapValues.columns)
	
class AutoXGBoostImputer(AutoXGBoost):
	"""This is an imputer. It imputs missing values into dataset based on XGBoost-trained model"""
	def fillMissing(self, cn:str, dmat=None):
		"""Finds values missing in the column, predicts them and imputs."""
		if dmat is None:
			dmat=self
		sel=dmat.colIsNA(cn)
		v=dmat.pds.loc[sel]
		if len(v):
			res=self.predict(cn, dmat=v, argmax=False)
			if cn in self.groups["categorical"]:
				cn=self.catRemap[cn]
			dmat.pds.loc[sel, cn]=res
	
	def imput(self, columns:Iterable[str]=None, dmat=None):
		"""Imputs values into all the columns iteratively starting from the best-performing models."""
		if dmat is None:
			dmat=self
		if columns is None:
			columns = set(self.scores)
		
		stoppedColumns = columns & self.groups["stop"]
		if stoppedColumns:
			warnings.warn("Some columns to be processed are in `stop` category. " + repr(stoppedColumns) + " Ignoring them.")
			columns -= stoppedColumns
		
		with mtqdm(columns, desc="imputting") as pb:
			for cn in pb:
				self.fillMissing(cn, dmat=dmat)